import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// 경로 별칭 설정   // https://vitejs-kr.github.io/config/
export default defineConfig({
  plugins: [vue()]
})
