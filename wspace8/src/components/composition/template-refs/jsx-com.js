export default {
    setup() {
        const root = ref(null)

        return () =>
            h('div', {
                ref: root
            })

        // return () => <div ref={root} />
    }
}
