export default {
    namespaced: true,
    state: {
        framework: 'Vue3',
        webpack: 'Vite2',
        design: 'quasar'
    },
    mutations: {
        framework(state, data) {
            state.framework = data;
        }
    },
    getters: {
        framework(state) {
            return state.framework;
        }
    },
    actions: {
        framework({ commit }) {
            return new Promise( (resolve, reject) => {
                resolve(true);
            })
        }
    }
}
