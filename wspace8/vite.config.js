// import { defineConfig, loadEnv } from 'vite'
// import vue from '@vitejs/plugin-vue'
// import { quasar, transformAssetUrls } from '@quasar/vite-plugin'
// // import auth from './src/store/modules/auth.js'
//
// // https://vitejs.dev/config/
// export default ({mode}) => {
//   /**
//    * loadEnv(mode: string, envDir: string, prefixes?: string | string[])
//    */
//   process.env = {...process.env, ...loadEnv(mode, process.cwd())};
//
//   return defineConfig({
//     server: {
//       host: '0.0.0.0',
//       port: process.env.VITE_SERVER_PORT,
//       proxy: {
//         '/api': process.env.VITE_API_URL,
//       }
//     },
//     plugins: [
//       vue({
//         template: { transformAssetUrls }
//       }),
//       quasar({
//         sassVariables: 'src/assets/quasar/quasar-variables.sass'
//       }),
//       // auth() //실행시 최초 1회 실행...
//     ]
//   })
// }
// export default defineConfig( {
//   plugins: [vue()]
// })

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

export default ({mode}) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  return defineConfig({
    server: {
      // host: '0.0.0.0',
      // port: process.env.VITE_SERVER_PORT,
      proxy: {
        '/api': process.env.VITE_API_URL,
      }
    },
    plugins: [
        vue({
          template: { transformAssetUrls }
        }),
        quasar({
          sassVariables: 'src/assets/quasar/quasar-variables.sass'
        })
    ]
  })
}

